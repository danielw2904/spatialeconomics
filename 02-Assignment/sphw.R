## Neighbors
neigh <- c(1, 2, 3, 3, 3, 4, 5, 5, 5, 6, 7, 7)
neigh2 <-c(2, 3, 4, 5, 6, 5, 6, 7, 8, 7, 8, 9)

neigh3 <- c(1,1,2,2,2,2,3,3,3,4,4,4,4,5,5,5,5,6,6,6,7,7)
neigh4 <- c(2,3,3,4,5,6,4,5,6,5,6,7,8,6,7,8,9,7,8,9,8,9)

W <- matrix(0, 9, 9)

assign1 <- function(x,y){
  W[x,y] <<- 1
}

mapply(assign1, neigh, neigh2)

W <- W + t(W)

row_stdz <- function(W){
  sums <- apply(W, 1, sum)
  return(W / sums)
}

W <- row_stdz(W)

W
